# PPW Kelompok ARAH
## Members
1. Ali Asadillah
2. Hanrichie
3. Muzaki Azami K
4. Riri Edwina 

## Link
https://ar-ah.herokuapp.com/

## About our application
Our apps helps college students finding everything they need academically, so they can focus on the thing that matters, using them.
The apps will give college students, depending on their major, a list of apps, books, and websites that they need. 
Users can submit their own reccomendation of apps/webs/books. Every recommendations can be rated by the users, to keep the quality
of the recommendations in check.

## Implemented Features
- Recommendation submission for several types of categories (Webs,Books,Applications)
- Developers' page that links to our respective online portfolio
