from django.test import TestCase,Client
from .models import WebsitesSci,WebsitesSoc
from .views import *
from .urls import *

# Create your tests here.
class SocialWebsUnitTest(TestCase):
    def test_SocWeb_exists(self):
        WebsitesSoc.objects.create(title="A",theurl="X")
        howmany = WebsitesSoc.objects.all().count()
        self.assertEqual(howmany,1)


class ScienceWebsUnitTest(TestCase):
    def test_SciWeb_exists(self):
        WebsitesSci.objects.create(title="A",theurl="X")
        howmany = WebsitesSci.objects.all().count()
        self.assertEqual(howmany,1)


class TemplateAcces(TestCase):
    def test_scienceWebs_accessed(self):
        satu = Client().get('/webs/science/')
        self.assertTemplateUsed(satu,'scienceWebs.html')
    
    def test_socialWebs_accessed(self):
        response = Client().get('/webs/social/')
        self.assertTemplateUsed(response,'socialWebs.html')
    
    def test_sciWebsRec_accessed(self):
        response = Client().get('/webs/science/recommendation/')
        self.assertTemplateUsed(response,'sciWebsRec.html')

    def test_socWebsRec_accessed(self):
        response = Client().get('/webs/social/recommendation/')
        self.assertTemplateUsed(response,'socWebsRec.html')
    
    def test_social_url_is_exist(self):
        response = Client().get('/webs/social/')
        self.assertEqual(response.status_code, 200)