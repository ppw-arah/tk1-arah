from django import forms
from . import models


class AddAppSci(forms.ModelForm):
    class Meta:
        model = models.Applications
        fields = ['title','theurl']

class AddAppSoc(forms.ModelForm):
    class Meta:
        model = models.Applications2
        fields = ['title','theurl']