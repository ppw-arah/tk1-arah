from django.shortcuts import render,redirect
from .models import Applications,Applications2    ##CHANGE
from . import forms

# Create your views here.


## Social Apps
def socApp(request):
    title = "SocApp"
    applications = Applications2.objects.all()
    return render(request,'socialApps.html', {"title": title,"applications":applications})

def upvoteSocApp(request,id):
    socApp = Applications2.objects.get(id=id)
    socApp.score+=1
    socApp.save()
    return redirect("apps:socApp")

def downvoteSocApp(request,id):
    socApp = Applications2.objects.get(id=id)
    socApp.score-=1
    socApp.save()
    return redirect("apps:socApp")

def socAppRec(request):
    title = "SocAppRec"
    if request.method == 'POST':
        form = forms.AddAppSoc(request.POST)
        if form.is_valid():
            form.save()
            form = forms.AddAppSoc()
            return redirect("apps:socApp")

    else:
        form = forms.AddAppSoc()
    return render(request, 'socAppsRec.html', {"title" :title,"form" : form})

## Science Apps
def sciApp(request):
    title = "SciApp"
    applications = Applications.objects.all()
    return render(request,'scienceApps.html', {"title": title,"applications":applications})


def upvoteSciApp(request,id):
    sciApp = Applications.objects.get(id=id)
    sciApp.score+=1
    sciApp.save()
    return redirect("apps:sciApp")

def downvoteSciApp(request,id):
    sciApp = Applications.objects.get(id=id)
    sciApp.score-=1
    sciApp.save()
    return redirect("apps:sciApp")

def sciAppRec(request):
    title = "SciAppRec"
    if request.method == 'POST':
        form = forms.AddAppSci(request.POST)
        if form.is_valid():
            form.save()
            form = forms.AddAppSci()
            return redirect("apps:sciApp")

    else:
        form = forms.AddAppSci()
    return render(request, 'sciAppsRec.html', {"title" :title,"form" : form})
